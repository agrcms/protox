<?php

/**
 * @file
 * Module file for wxt_overrides, handle head meta requirements for tbs plus more.
 */

use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\Core\Form\FormStateInterface;
use Drupal\agri_admin\AgriAdminHelper;
use Drupal\agri_admin\Utils;

/**
 * Implements hook_ckeditor_settings_alter().
 *
 * Provide a custom css file for CKEditor version 4.
 */
function wxt_overrides_ckeditor_settings_alter(array &$page) {
  $path = \Drupal::service('extension.list.module')->getPath('wxt_overrides');
  // Load the followus css file in CKEditor.
  $settings['contentsCss'] = [
    $path . '/css/followus.css',
  ];
}


/**
 * Implements hook_page_attachments().
 *
 * Provide aafc metatag elements in the head to each page.
 */
function wxt_overrides_page_attachments(array &$page) {

  $debug = FALSE;
  if ($debug && $fp = fopen('modules/custom/wxt_overrides/debug.txt', 'a')) {
    fwrite($fp, print_r($page, TRUE) . "\n");
    fclose($fp);
  }

  $metatags_module_head = $page['#attached']['html_head'];
  if (!isset($metatags_module_head)) {
    return;
  }
  $langId = \Drupal::languageManager()->getCurrentLanguage()->getId();
  // Add page meta.
  foreach ($metatags_module_head as $key => $value) {
    if (isset($value[0]['#attributes']['name'])) {
      $name = $value[0]['#attributes']['name'];
      if ($name == 'dcterms.language') {
        unset($page['#attached']['html_head'][$key]);
      }
    }
  }

  $dctermsaccessRights = [
    '#tag' => 'meta',
    '#attributes' => [
      'name'  => 'dcterms.accessRights',
      'content'   => '2',
    ],
  ];

  if ($langId == 'en') {
    $dctermslanguage = [
      '#tag' => 'meta',
      '#attributes' => [
        'name'  => 'dcterms.language',
        'title'     => 'ISO639-2',
        'content'   => 'eng',
      ],
    ];

    $dctermsservice = [
      '#tag' => 'meta',
      '#attributes' => [
        'name'  => 'gcaaterms.sitename',
        'content'   => 'AAFC-AAC',
      ],
    ];

  }

  if ($langId == 'fr') {
    $dctermslanguage = [
      '#tag' => 'meta',
      '#attributes' => [
        'name'  => 'dcterms.language',
        'title'     => 'ISO639-2',
        'content'   => 'fra',
      ],
    ];

    $dctermsservice = [
      '#tag' => 'meta',
      '#attributes' => [
        'name'  => 'gcaaterms.sitename',
        'content'   => 'AAFC-AAC',
      ],
    ];
  }

  $page['#attached']['html_head'][] = [$dctermsaccessRights,'dcterms.accessRights'];
  $page['#attached']['html_head'][] = [$dctermslanguage,'dcterms.language'];
  $page['#attached']['html_head'][] = [$dctermsservice,'gcaaterms.sitename'];

  $node = \Drupal::routeMatch()->getRawParameter('node');
  if (!isset($node)) {
    $webform = \Drupal::routeMatch()->getRawParameter('webform');
    if (isset($webform)) {
      // Implement the logic by adding the hard codes for meta data (dcterms.issued and dcterms.modified).
      // The team decided to use the hard code approach in the short term.
      if (($webform == 'yourfeedback_programsandservices') || ($webform == 'aafc_generic_contact_form')) {
        $datestr = '2021-06-23';
        $dctermsissued = [
          '#tag' => 'meta',
          '#attributes' => [
            'name'  => 'dcterms.issued',
            'title'     => 'W3CDTF',
            'content'   => $datestr,
          ],
        ];

        $dctermsmodified = [
          '#tag' => 'meta',
          '#attributes' => [
            'name'  => 'dcterms.modified',
            'title'     => 'W3CDTF',
            'content'   => $datestr,
          ],
        ];
      }
      elseif (($webform == 'request_agriinvest_form_excel') || ($webform == 'as_ai_request_form')) {
        $datestr = '2022-02-05';
        $dctermsissued = [
          '#tag' => 'meta',
          '#attributes' => [
            'name'  => 'dcterms.issued',
            'title'     => 'W3CDTF',
            'content'   => $datestr,
          ],
        ];

        $dctermsmodified = [
          '#tag' => 'meta',
          '#attributes' => [
            'name'  => 'dcterms.modified',
            'title'     => 'W3CDTF',
            'content'   => $datestr,
          ],
        ];
      }
      else {
        // set a default value.
        $datestr = '2022-02-05';
        $dctermsissued = [
          '#tag' => 'meta',
          '#attributes' => [
            'name'  => 'dcterms.issued',
            'title'     => 'W3CDTF',
            'content'   => $datestr,
          ],
        ];

        $dctermsmodified = [
          '#tag' => 'meta',
          '#attributes' => [
            'name'  => 'dcterms.modified',
            'title'     => 'W3CDTF',
            'content'   => $datestr,
          ],
        ];
      }

      $page['#attached']['html_head'][] = [$dctermsissued, 'dcterms.issued'];
      $page['#attached']['html_head'][] = [$dctermsmodified, 'dcterms.modified'];
    }
    else {
      return;
    }
  }

  // Add page meta.
  $summary = '';
  $route_match = Utils::getRouteName();

  if (($route_match == 'entity.node.canonical' || $route_match == 'entity.node.revision' || $route_match == 'entity.node.latest_version') &&
       isset($page['#attached']['html_head'])) {
    $metatags_module_head = $page['#attached']['html_head'];
    foreach ($metatags_module_head as $key => $value) {
      if (isset($value[0]['#attributes']['name'])) {
        $name = $value[0]['#attributes']['name'];

        if ($name == 'dcterms.description') {
          $summary = $value[0]['#attributes']['content'];
          unset($page['#attached']['html_head'][$key]);
        }

        if ($name == 'keywords') {
          $content = $value[0]['#attributes']['content'];
          $keywords = [
            '#tag' => 'meta',
            '#attributes' => [
              'name'  => 'keywords',
              'content'   => $content,
            ],
          ];
          unset($page['#attached']['html_head'][$key]);
        }

        if ($name == 'dcterms.creator') {
          $content = $value[0]['#attributes']['content'];
          $dctermscreator = [
            '#tag' => 'meta',
            '#attributes' => [
              'name'  => 'dcterms.creator',
              'content'   => $content,
            ],
          ];
          unset($page['#attached']['html_head'][$key]);
          $page['#attached']['html_head'][] = [$dctermscreator,'dcterms.creator'];
        }

        if ($name == 'dcterms.title') {
          $content = $value[0]['#attributes']['content'];
          $dctermstitle = [
            '#tag' => 'meta',
            '#attributes' => [
              'name'  => 'dcterms.title',
              'content'   => $content,
            ],
          ];
          unset($page['#attached']['html_head'][$key]);
        }

        if ($name == 'dcterms.type') {
          $content = $value[0]['#attributes']['content'];
          $dctermstype = [
            '#tag' => 'meta',
            '#attributes' => [
              'name'  => 'dcterms.type',
              'title'     => 'gctype',
              'content'   => $content,
            ],
          ];
          unset($page['#attached']['html_head'][$key]);
        }

        if ($name == 'dcterms.relation') {
          $content = $value[0]['#attributes']['content'];
          $aafcsubject = [
            '#tag' => 'meta',
            '#attributes' => [
              'name'  => 'aafc.subject',
              'title'     => 'aafcsubject',
              'content'   => $content,
            ],
          ];
          unset($page['#attached']['html_head'][$key]);
        }

        if ($name == 'dcterms.subject') {
          $content = $value[0]['#attributes']['content'];
          $dctermssubject = [
            '#tag' => 'meta',
            '#attributes' => [
              'name'  => 'dcterms.subject',
              'title'     => 'gccore',
              'content'   => $content,
            ],
          ];
          unset($page['#attached']['html_head'][$key]);
        }
      }
    }
  }

  if ($node) {
    $node = \Drupal::routeMatch()->getParameter('node');
    if (!$node) {
      return;
    }
    if (is_string($node)) {
      // Occurs when reverting to a previous version.
      return;
    }

    $issued_date = '';
    $modified_date = '';
    if (!$node->hasField('field_issued')) {
      return;
    }
    if ($node->get('field_issued')->first()) {
      $issued_date = $node->get('field_issued')->first()->date->format('Y-m-d');
    }
    if ($node->get('field_modified')->first()) {
      $modified_date = $node->get('field_modified')->first()->date->format('Y-m-d');
    }

    if ($node->get('field_description')->first()) {
      $field_description_value = $node->get('field_description')->first()->getString();
    }
    if (isset($field_description_value)) {
      if (strlen($field_description_value) == strlen($summary)) {
        $summary_description = $field_description_value;
      }
      else {
        if (strpos($field_description_value, $summary) !== false) {
          $summary_description = $field_description_value;
        }
        else {
          $summary_description = $field_description_value . '; '. $summary;
        }
      }
    }
    else {
      $summary_description = $summary;
    }

    $dctermsdescription = [
      '#tag' => 'meta',
      '#attributes' => [
        'name'  => 'dcterms.description',
        'content'   => $summary_description,
      ],
    ];

    if (!isset($keywords)) {
      $keywords = [
        '#tag' => 'meta',
        '#attributes' => [
          'name'    => 'keywords',
          'content' => '',
        ],
      ];
    }

    if ($langId == 'en') {
      $dcterms_title_override = $node->get('field_dcterms_title_override_eng')->getString();
    }
    elseif ($langId == 'fr') {
      $dcterms_title_override = $node->get('field_dcterms_title_override_fra')->getString();
    }

    if (!empty($dcterms_title_override)) {
      $dctermstitle = [
        '#tag' => 'meta',
        '#attributes' => [
          'name'  => 'dcterms.title',
          'content'   => $dcterms_title_override,
        ],
      ];
    }

    if (!isset($dctermstype)) {
      $dctermstype = [
        '#tag' => 'meta',
        '#attributes' => [
          'name'  => 'dcterms.type',
          'title'     => 'gctype',
          'content'   => '',
        ],
      ];
    }

    if (!isset($aafcsubject)) {
      $aafcsubject = [
        '#tag' => 'meta',
        '#attributes' => [
          'name'  => 'aafc.subject',
          'title'     => 'aafcsubject',
          'content'   => '',
        ],
      ];
    }

    if (!isset($dctermssubject)) {
      $dctermssubject = [
        '#tag' => 'meta',
        '#attributes' => [
          'name'  => 'dcterms.subject',
          'title'     => 'gccore',
          'content'   => '',
        ],
      ];
    }

    $dctermsissued = [
      '#tag' => 'meta',
      '#attributes' => [
        'name'  => 'dcterms.issued',
        'title'     => 'W3CDTF',
        'content'   => $issued_date,
      ],
    ];

    $dctermsmodified = [
      '#tag' => 'meta',
      '#attributes' => [
        'name'  => 'dcterms.modified',
        'title'     => 'W3CDTF',
        'content'   => $modified_date,
      ],
    ];

    if (isset($dctermstitle)) {
      $page['#attached']['html_head'][] = [$dctermstitle, 'dcterms.title'];
    }
    $page['#attached']['html_head'][] = [$dctermsissued, 'dcterms.issued'];
    $page['#attached']['html_head'][] = [$dctermsmodified, 'dcterms.modified'];
    $page['#attached']['html_head'][] = [$dctermstype, 'dcterms.type'];
    $page['#attached']['html_head'][] = [$aafcsubject, 'aafc.subject'];
    $page['#attached']['html_head'][] = [$dctermssubject, 'dcterms.subject'];
    $page['#attached']['html_head'][] = [$keywords, 'keywords'];
    $page['#attached']['html_head'][] = [$dctermsdescription, 'dcterms.description'];
  }

  $dctermsspatial = [
    '#tag' => 'meta',
    '#attributes' => [
      'name'  => 'dcterms.spatial',
      'title'     => 'gcregions',
      'content'   => '',
    ],
  ];

  $dctermsaudience = [
    '#tag' => 'meta',
    '#attributes' => [
      'name'  => 'dcterms.audience',
      'title'     => 'gcaudience',
      'content'   => '',
    ],
  ];

  $dctermsformat = [
    '#tag' => 'meta',
    '#attributes' => [
      'name'  => 'dcterms.format',
      'title'     => 'gcformat',
      'content'   => '',
    ],
  ];

  $page['#attached']['html_head'][] = [$dctermsaudience, 'dcterms.audience'];
  $page['#attached']['html_head'][] = [$dctermsformat, 'dcterms.format'];
  $page['#attached']['html_head'][] = [$dctermsspatial, 'dcterms.spatial'];
}

/**
 * Implements hook_form_node_form_alter().
 */
function wxt_overrides_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  /* set the default value for communictaion as meta data subject type */
  if ($form_id == 'node_news_edit_form') {
    // Communication id =38.
    $communicationid = 38;
    $form['field_subject']['widget']['#options_attributes'][$communicationid] = ['checked' => 'checked'];
  }

  if ($form_id == 'node_landing_page_edit_form' || $form_id == 'node_page_edit_form'  ||
      $form_id == 'node_landing_page_form'      || $form_id == 'node_page_form'  ||
      $form_id == 'page-node-type-empl'  || $form_id == 'page-node-type-news') {

    foreach ($form['menu']['link']['menu_parent']['#options'] as $key => &$value) {
      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
      if ($langcode == 'en' && strpos($value, 'disabled') == FALSE) {
        $keyarray = explode(":", $key);
        if (count($keyarray) > 2) {
          if (AgriAdminHelper::isLinkFrenchOnly($keyarray[2])) {
            unset($form['menu']['link']['menu_parent']['#options'][$key]);
          }
        }
      }
      else {
        if ($langcode == 'fr' && strpos($value, 'désactivé') == FALSE) {
          $keyarray = explode(":", $key);
          if (count($keyarray) > 2) {
            if (AgriAdminHelper::isLinkEnglishOnly($keyarray[2])) {
              unset($form['menu']['link']['menu_parent']['#options'][$key]);
            }
          }
        }
      }

      if (strpos($value, '-- ') == 0) {
        // $value =  substr($value,0, 3) . 'level 1. ' . substr( $value,3, strlen($value)) ;
        $form['menu']['link']['menu_parent']['#options_attributes'][$key]['class'] = ['class' => 'menu_tree-leve-1'];
      }
      else {
        if (strpos($value, '---- ') == 0) {
          // $value =  substr($value,0, 5) . 'level 2. ' . substr( $value,5, strlen($value)) ;
          $form['menu']['link']['menu_parent']['#options_attributes'][$key]['class'] = 'menu_tree-leve-2 glyphicon';
        }
        else {
          if (strpos($value, '------ ') == 0) {
            // $value =  substr($value,0, 7) . 'level 3. ' . substr( $value,7, strlen($value)) ;
            $form['menu']['link']['menu_parent']['#options_attributes'][$key]['class'] = 'menu_tree-leve-3';
          }
          else {
            if (strpos($value, '-------- ') == 0) {
              // $value =  substr($value,0, 9) . 'level 4. ' . substr( $value,9, strlen($value)) ;
              $form['menu']['link']['menu_parent']['#options_attributes'][$key]['class'] = 'menu_tree-leve-4';
            }
            else {
              if (strpos($value, '---------- ') == 0) {
                // $value =  substr($value,0, 11) . 'level 5. ' . substr( $value,11, strlen($value)) ;
                $form['menu']['link']['menu_parent']['#options_attributes'][$key]['class'] = 'menu_tree-leve-5';
              }
              else {
                if (strpos($value, '------------ ') == 0) {
                  // $value =  substr($value,0, 13) . 'level 6. ' . substr( $value,11, strlen($value)) ;
                  $form['menu']['link']['menu_parent']['#options_attributes'][$key]['class'] = 'menu_tree-leve-6';
                }
              }
            }
          }
        }
      }
    }
  }
  $entity = $form_state->getFormObject()->getEntity();
  $content_type = $entity->bundle();
  if ($content_type == 'page' || $content_type == 'landing_page' || $content_type == 'empl' || $content_type == 'news') {
    $other_lang = AgriAdminHelper::getOtherLang();
    if (isset($form['moderation_state']['widget'][0]['state']['#default_value'])) {
      $current_state = $form['moderation_state']['widget'][0]['state']['#default_value'];
      if ($current_state == 'published') {
        $form['moderation_state']['widget'][0]['state']['#default_value'] = 'draft';
        $form['moderation_state'][$other_lang]['widget'][0]['state']['#default_value'] = 'draft';
      }
    }
  }
}

/**
 * Implements hook_menu_alter().
 */
function wxt_overrides_link_alter(&$variables) {
  $url = $variables['url'];

  // Return early where possible.
  if (!$url->isRouted()) {
    return;
  }
  else {
    $route_name = $url->getRouteName();
    // $parent_route = $url->getParentRouteName();
    $current_route = \Drupal::routeMatch()->getRouteName();
    $user = User::load(\Drupal::currentUser()->id());
    $moderated_default = $user->get('field_default_content_mode')->first();
    if (!empty($moderated_default)) {
      $moderated_default = $moderated_default->getValue()['value'];
    }
    else {
      $moderated_default = FALSE;
    }
    if ($current_route == 'system.admin_content' && $variables['text'] == 'Overview') {
      return;
    }
    elseif ($current_route == 'content_moderation.admin_moderated_content' && $variables['text'] == 'Overview') {
      return;
    }
    elseif ($variables['text'] == 'Overview') {
      return;
    }
    elseif ($moderated_default && $route_name == 'system.admin_content' && $variables['text'] != 'Overview') {
      $link_options = $url->getOptions();
      if (isset($link_options['attributes']['class'][1])) {
        foreach ($link_options['attributes']['class'] as $key => $classname) {
          if ($classname === 'tabs__link') {
            // Do not manipulate this tabs__link as it is otherwise confusing to the end user.
            return;
          }
        }
      }
      $debug = FALSE;
      if ($debug && $fp = fopen('modules/custom/wxt_overrides/debug2.txt', 'a')) {
        // fwrite($fp, print_r(get_class_methods($url), TRUE) . "\n");.
        fwrite($fp, print_r($url->getOptions(), TRUE) . "\n");
        fclose($fp);
      }
      $variables['text'] = t('Moderated content');
      $variables['url'] = Url::fromUri('internal:/admin/content/moderated');
    }
    elseif ($variables['text'] == 'Table') {
      return;
    }
    elseif ($route_name == 'entity.media.collection') {
      $link_options = $url->getOptions();
      if ($current_route == 'entity.media.collection') {
        if (in_array('tabs__link', $link_options['attributes']['class'])) {
          // Do not manipulate this tabs__link as it is otherwise confusing to the end user.
          return;
        }
      }
      if (isset($link_options['attributes']['class']) && in_array('tabs__link', $link_options['attributes']['class']) && $variables['text'] == 'Table') {
        // Do not manipulate this tabs__link as it is otherwise confusing to the end user.
        return;
      }
      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $variables['text'] = 'Media (grid)';
      if ($langcode == 'fr') {
        $variables['text'] = 'Médias (Grille)';
      }
      $variables['url'] = Url::fromUri('internal:/admin/content/media-grid');
    }
  }
}

/**
 * Implements hook_form_alter().
 *
 * Handle webform submission feedback form and the aafc generic contact form.
 */
function wxt_overrides_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'webform_submission_yourfeedback_programsandservices_add_form') {
    // Discussed passing PS id through the request query string, Huan approved this approach.
    $ps_id = \Drupal::request()->query->get('ps_id');
    // Set the default Program or Service based on calling program page.
    $form['elements']['programorservice']['#default_value'] = $ps_id;
  }
  if ($form_id == 'webform_submission_aafc_generic_contact_form_add_form') {
    $langId = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($langId == 'en') {
      $form['elements']['preferredresponselanguage']['#default_value'] = 'English';
    }
    elseif ($langId == 'fr') {
      $form['elements']['preferredresponselanguage']['#default_value'] = 'French';
    }
  }
}

/**
 * Implements hook_cron().
 */
function wxt_overrides_cron() {
  // $view = \Drupal\views\Views::getView('broken_links_report');
  // $views_preview = $view->preview('data_export_1');
  // $display = \Drupal::service('renderer')->renderRoot($views_preview);
  // $directory = 'public://linkcheck';
  // $file_system = \Drupal::service('file_system');
  // $file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
  // $file_system->saveData($display, 'public://linkcheck/links.json', FileSystemInterface::EXISTS_REPLACE);
}
