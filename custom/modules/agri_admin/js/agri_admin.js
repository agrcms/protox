/**
 * @file
 * AAFC Agri Admin behaviors.
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.agriAdmin = {
    attach: function (context, settings) {
      AAFCOnline.sortMediaDisplayModes('[data-drupal-selector="edit-attributes-data-view-mode"]'); // Call this for all attach events.
      AAFCOnline.sortMediaDisplayModes('[data-drupal-selector="edit-images-thumbnail-image-style"]'); // Call this for all attach events.
      // build a Drupal modal dialog window
      var msg_part1 =  Drupal.t('If you click ok, the archived page can\'t be restored.');
      var msg_part2 =  Drupal.t('Are you sure you want to delete it?');
      var content  = '<div><p id="version-confirm-form-text">' + msg_part1 +'<br/>' + msg_part2 + '</p></div>';
      confirmationDialog = Drupal.dialog(content, {
        dialogClass: 'confirm-dialog',
        resizable: false,
        closeOnEscape: false,
        width:500,
        title: Drupal.t('Delete an archived page?'),
        buttons: [{
          text: Drupal.t('Yes'),
          class: 'button button--primary',
          click: function click(e) {
            confirmationDialog.close();
            $(".node-form #edit-submit").unbind('click.agriAdmin');
            $(".node-form #edit-submit").trigger('click.agriAdmin');
            $(e.target).remove();
            return true;
          },
          primary: true
        }, {
          text: Drupal.t('Cancel'),
          class: 'button',
          click: function click() {
            confirmationDialog.close();
          }
        }],
        create: function () {
        },
        beforeClose: false,
        close: function (event) {
          $(event.target).remove();
        }
      });
      if (context == document) {
        AAFCOnline.init();
        if ($('body').hasClass('user-logged-in')) {
          if ($('body').hasClass('path-node')) {
            $('#edit-submit').bind('click.agriAdmin', function(e) {
              // Get the current state. Need to clone this object and remove the label so that we can get just the state.
              var cur_state = '';
              var mod_state = $('#edit-moderation-state-0-current').clone();
              if (mod_state) {
                $('label', mod_state).remove();
                var cur_state = $(mod_state).text().trim();
                cur_state = Drupal.t(cur_state);
              }
              var new_state = $('#edit-moderation-state-0-state option:selected').text();
              new_state = Drupal.t(new_state);
              // Remove the message 'Are you sure you want to revive this page?': moderation state is changed from archived to draft.
              if ((cur_state == Drupal.t('Archived')) && (new_state == Drupal.t('Delete'))) {
                var confirm_message = Drupal.t('If you click ok, the archived page can\'t be restored.\nAre you sure you want to delete it?');
                e.preventDefault();
                confirmationDialog.showModal();
                return false;
              }
              return true;
            });
          }
          // Check if the actual 'admin' user is logged in, based on the name displayed in the toolbar.
          // There is a small delay before this information is available, so set a timer.
          // If this needs to be based on the admin _role_ instead, then we'll need an API function for that.
          if ($('body').hasClass('node-edit') && $('body').hasClass('node-add')) {
            setTimeout(function() {
              var user = $('#toolbar-item-user').text();
              if (user != 'admin') {
                //$('#edit-field-meta-tags-0').hide(); // Hide the META TAGS tab in the node editor
                //$('#edit-field-meta-tags-etuf-fr-0').hide(); //because now weh have the ETUF (fr and en form)
                //$('#edit-author').hide(); //Hide the Authoring Information.
                //$('#edit-meta-author').hide();
                //$('#edit-revision-information').hide();
                //$('#edit-content-translation').hide();
                $('.js-form-item-promote-value.form-item-promote-value').hide();
                $('.js-form-item-promote-etuf-fr-value.form-item-promote-etuf-fr-value').hide();
                console.log('agri_admin hide the promote checkbox on node edit.');
              }
            }, 500);
          }

          //make sure in ETUF the french language is french (it defaults to english if creating node in french UI)
          if (AAFCOnline.lang == 'fr') {
            $('#edit-langcode-0-value option[value="en"]').removeAttr("selected");
            $('#edit-langcode-0-value option[value="fr"]').attr("selected","selected");
          }


          //ETUF - sync the publish status selection, with JS
          var bothStatus = $("#edit-moderation-state-0-state, #edit-moderation-state-etuf-fr-0-state, #edit-moderation-state-etuf-en-0-state");
          bothStatus.change(function(e) {
            bothStatus.val(this.value); // "this" is the changed one
          });

          // Change the submit button value to "Save" instead of "Save (this translation)"
          if ($('body').hasClass('i18n-en')) {
            $(".node-form .form-actions #edit-submit").val("Save");
          }

          // Hide disabled menu links
          if ($('body').hasClass('adminstructuremenumanagesideb') || $('body').hasClass('adminstructuremenumanagemain')) {
            var disabledMenuLinkHtml = '<form id="menu-disabled-links-form" action="#nothing">' +
            '<input type="checkbox" id="menu-disabled-links-switch" name="menu-disabled-links-switch" value="enabled" checked>' +
            '<label for="menu-disabled-links-switch" class="show-disabled"> ' + Drupal.t('Hide disabled items') + '</label>' +
            '</form>';
            if ($('#menu-disabled-links-form').length < 1) {
              $('div.region-content .tabledrag-toggle-weight-wrapper').prepend(disabledMenuLinkHtml);
              $('table#menu-overview tr.menu-disabled').each(function(index, element) {
                $(this).hide();
              });
              $('#menu-disabled-links-switch label.hide-disabled').hide();
              $("#menu-disabled-links-switch").click(function(e) {
                $('table#menu-overview tr.menu-disabled').each(function(index, element) {
                  if ($(element).is(":hidden")) {
                    $(element).show();
                  }
                  else {
                    $(element).hide();
                  }
                });
              });
            }
          }
        }//end if user is loggedIn
      }
    }
  };
})(jQuery, Drupal, drupalSettings);


var AAFCOnline = function() {
  var initialized = false;   // Flag to indicate that this class has been initialized
  var form_required_valid = true; // Flag to indicate that the validation is for the News and EO forms
  var lang = 'en';           // Will be 'en' or 'fr' regardless of how the url segment is formed (currently eng or fra)
  var mouse = {x:0, y:0};    // Tracks the mouse position
  var page_type = 'content';

  /**
   * Initialization
   */
  function init() {
    if (initialized) {
      return;
    }

    // Get the current UI language
    $ = jQuery;
    AAFCOnline.lang = $('html').attr('lang');

    if (AAFCOnline.isIE()) {
      $('body').addClass('isIE');
    }

    //Determine the page type
    if ($('body').hasClass('path-frontpage')) {
      AAFCOnline.page_type = 'front'; // Front page <front> ex, /en or /fr.
    }
    else if ($('body').hasClass('employment-opportunity-request-form-page')) {
      AAFCOnline.page_type = 'add-empl-special';
    }
    else if ($('body').hasClass('news-article-form-page')) {
      AAFCOnline.page_type = 'add-news-special';
    }
    else if ($('body').hasClass('nodeaddlanding_page')) {
      AAFCOnline.page_type = 'add-landing-page';
    }
    else if ($('body').hasClass('nodeaddempl')) {
      AAFCOnline.page_type = 'add-empl';
    }
    else if ($('body').hasClass('nodeaddnews')) {
      AAFCOnline.page_type = 'add-news';
    }
    else if ($('body').hasClass('nodeaddpage')) {
      AAFCOnline.page_type = 'add-page';
    }
    else if ($('body').hasClass('node-edit') && $('body').hasClass('page-node-type-landing-page')) {
      AAFCOnline.page_type = 'edit-landing-page';
    }
    else if ($('body').hasClass('node-edit') && $('body').hasClass('page-node-type-page')) {
      AAFCOnline.page_type = 'edit-page';
    }
    else if ($('body').hasClass('node-edit') && $('body').hasClass('page-node-type-news')) {
      AAFCOnline.page_type = 'edit-news';
    }
    else if ($('body').hasClass('node-edit') && $('body').hasClass('page-node-type-empl')) {
      AAFCOnline.page_type = 'edit-empl';
    }
    else if ($('body').hasClass('path-webform')) {
      AAFCOnline.page_type = 'webforms';
    }
    else if ($('body').hasClass('grstheme-bootstrap') && $('body').hasClass('path-node')) {
      AAFCOnline.page_type = 'content'; // Content pages generated by grstheme_bootstrap.
    }
    else if ($('body').hasClass('path-admin')) {
      AAFCOnline.page_type = 'admin'; // Other admin page.
    }

    if (AAFCOnline.page_type == 'edit-page' ||
        AAFCOnline.page_type == 'add-page' ||
        AAFCOnline.page_type == 'add-landing-page' ||
        AAFCOnline.page_type == 'edit-landing-page'
    ) {
      var large_mode = $('#edit-field-large-value').bind('click.largeMode', function(event) {
        var layoutElement = $("#edit-layout-selection");
        if (typeof layoutElement !== 'undefined') {
          if (this.checked) {
            layoutElement.val('_none'); // Default to the default layout value.
          }
          else {
            if (AAFCOnline.page_type.includes('landing')) {
              layoutElement.val('node_landing_page_full_default'); // Default to the default layout value.
            }
            else {
              layoutElement.val('node_page_default_default'); // Default to the default layout value.
            }
          }
        }
      });
      
      setLayoutDefaults();
    }
    $(document).on('mousemove', onMouseMove);

    //remove the data-toggle from the links anchors in main navigation for media or large devices

    $('.alert a.access-unpublished').each(function(index, element) {
      if ($(this).hasClass('access-unpublished')) {
        // Add a copy-link icon to the unpublished link, if one exists
        var access_link = $('a.access-unpublished');
        if (access_link.length > 0) {
          var spn = $('<span class="glyphicon glyphicon-copy" style="margin-left: 10px" title="'+(AAFCOnline.lang=='fr'?'Copier le lien':'Copy link')+'"></span>');
          $(access_link).parent().append(spn);
          $(spn).click(function() {
            AAFCOnline.copyLink(window.location.origin + $(access_link).attr('href'));
          });
        }
      }
    });

    initialized = true;
  }


  function isIE() {
    if (navigator.appName == 'Microsoft Internet Explorer') {
      return true;
    }
    else if (navigator.appName == 'Netscape') {
      if (navigator.appVersion.indexOf('Trident') > 0 || navigator.appVersion.indexOf('Edge') > 0) {
        return true;
      }
    }
    return false;
  }


  /**
   * Keep track of mouse movements.
   */
  function setLayoutDefaults() {
    // Set the layout defaults.
    var large_mode = $('#edit-field-large-value').is(':checked');
    var layoutElement = $("#edit-layout-selection");
    if (AAFCOnline.page_type == 'add-page') {
      layoutElement.val('node_page_default_default'); // Default to the default layout value.
      if (large_mode) {
        layoutElement.val('_none'); // Default to the default layout value.
      }
    }
    if (AAFCOnline.page_type == 'add-landing-page') {
      layoutElement.val('node_landing_page_full_default'); // Default to the default layout value.
      if (large_mode) {
        layoutElement.val('_none'); // Default to the default layout value.
      }
    }

    if (AAFCOnline.page_type == 'edit-landing-page') {
      if (layoutElement.val() == '_none') {
        layoutElement.val('node_landing_page_full_default'); // Default to the default layout value.
      }
      if (large_mode) {
        layoutElement.val('_none'); // Default to the default layout value.
      }
    }
    if (AAFCOnline.page_type == 'edit-page') {
      if (layoutElement.val() == '_none') {
        layoutElement.val('node_page_default_default'); // Default to the default layout value.
      }
      if (large_mode) {
        layoutElement.val('_none'); // Default to the default layout value.
      }
    }
    //Layout - sync the layout type selection, with JS
    var bothSelects = $("#edit-layout-selection, #edit-layout-selection-etuf-fr, #edit-layout-selection-etuf-en");
    bothSelects.change(function(e) {
      bothSelects.val(this.value); // "this" is the changed one
    });

  }

  /**
   * Keep track of mouse movements.
   */
  function onMouseMove(event) {
    AAFCOnline.mouse.x = event.clientX;
    AAFCOnline.mouse.y = event.clientY;
  }

  /**
   * Copy a link to the clipboard
   */
  function copyLink(url) {
    if (isIE()) {
      window.clipboardData.setData('Text', url);
    }
    else {
      var range = document.createRange();
      var tmpElem = $('<div>');
      tmpElem.text(url);
      $('body').append(tmpElem);
      range.selectNodeContents(tmpElem.get(0));
      selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
      document.execCommand('copy', false, null);
      tmpElem.remove();
    }

    // Display a popup message to indicate the link was copied, then fade it out
    lang = AAFCOnline.lang;
    var copied_msg = $('<div>'+(lang=='fr'?'Copié':'Copied')+'</div>').css({
      'position': 'fixed',
      'background-color': 'black',
      'border-radius': '6px',
      'color': 'white',
      'padding': '10px 15px',
      'top': AAFCOnline.mouse.y,
      'left': AAFCOnline.mouse.x,
      'z-index': 10700
    })
    $('body').append(copied_msg);
    setTimeout(function() {
      $(copied_msg).animate({'opacity': 0}, 1000, function() {
        $(this).remove();
      });
    }, 1000);
  }


  /**
   * Original sort logic from https://riptutorial.com/jquery/example/11477/sorting-elements .
   */
  function sortMediaDisplayModes(selector) {
    // '[data-drupal-selector="edit-attributes-data-view-mode"]'
    var mediaStylesList = jQuery(selector);
    if (typeof mediaStylesList == 'undefined') {
      console.log('sortMediaDisplayModes trying to find mediaStylesList using selector ' + selector);
      return;
    }
    if (mediaStylesList) {
      var selected = jQuery(mediaStylesList).find('[selected="selected"]').detach();

      var styles = jQuery(mediaStylesList).children('option');
      var sortList = Array.prototype.sort.bind(styles);

      sortList(function(a, b) {
        // Cache inner content from the first element (a) and the next sibling (b)
        var aText = a.innerText;
        var bText = b.innerText;

        // Returning -1 will place element `a` before element `b`
        if ( aText < bText ) {
          return -1;
        }

        // Returning 1 will do the opposite
        if ( aText > bText ) {
          return 1;
        }

        // Returning 0 leaves them as-is
        return 0;
      });
      jQuery(mediaStylesList).append(jQuery(styles));
      jQuery(mediaStylesList).prepend(jQuery(selected));
    }

  }


  /**
   * Expose functions and variables
   */
  return {
    init: init,
    lang: lang,
    isIE: isIE,
    mouse: mouse,
    page_type: page_type,
    copyLink: copyLink,
    sortMediaDisplayModes: sortMediaDisplayModes,
  }
}();

